







A. TYPE DE DONNEE ABSTRAIT
Soit [a,b] un intervalle de la droite r�elle, a <= b ;
d�finissez le type abstrait intervalle et
proposer une classe c++ pour ce type 
(penser � impl�menter les fonctions +, -, x)
Aide : soit A=[a,b], B=[c,d] ; A+B = [a+b,c+d], A-B = [a-c,b-d],
AxB = [min{axc,axd,bxc,bxd}, max{axc,axd,bxc,bxd}]


Type 
operations
new : RxR --> interval
+ : interval x interval --> interval
- : interval x interval --> interval
* : interval x interval --> interval

Axiome
I(a,b) : interval, (a,b) in  RxR
(a<=b) is true







int* addition(int a, int b)
{
int* s = new int;
*s=a+b;
return s;
}
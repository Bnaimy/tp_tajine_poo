#include "pch.h"
#include "interval.h"


interval::interval( float a, float b)
{
	// a<=b
	assert(a <= b); // axiome
	// if(a>b) --> abort();
	// certifier que (a<=b)
	try
	{
		if (a > b) throw "PB DE BORNES"; // const char*
		this->binf = a;
		this->bsup = b;
	}
	catch (const char* e )
	{
		cout << e << endl;
	}
}


interval::~interval()
{
}

interval interval::operator+(const interval & i) const
{
	float a = this->binf + i.binf;
	float b = this->bsup + i.bsup;

	interval s(a, b);
	return s;
}

interval interval::operator-(const interval & i) const
{
	float a = this->binf - i.binf;
	float b = this->bsup - i.bsup;

	interval d(a, b);
	return d;
}

interval interval::operator*(const interval & i) const
{
	float a = min(min(binf*i.binf,binf*i.bsup ), min(bsup*i.binf,bsup*i.bsup ));
	float b = max(max(binf*i.binf, binf*i.bsup), max(bsup*i.binf, bsup*i.bsup));

	interval p(a, b);
	return p;
}

void interval::main()
{
	interval i(0, 0);
	i.print();
	(i + i).print();
	(i - i).print();
	(i*i).print();

}

void interval::print() const
{
	cout << "[" << binf << "," << bsup << "]" << endl;
}

interval interval::operator*(float alpha) const
{
	//this->binf = 0;
	float a = this->binf*alpha;
	float b = this->bsup*alpha;
	
	interval p(0, 0);
	if (alpha >= 0)
	{
		p.binf = a;
		p.bsup = b;
	}
	else 
	{
		p.binf = b;
		p.bsup = a;
	}
	
	return p;
}

float min(float a, float b)
{
	if (a < b) return a;
	else
	return b;
}

float max(float a, float b)
{
	if (a > b) return a;
	else
		return b;
}

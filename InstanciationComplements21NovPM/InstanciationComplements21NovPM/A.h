#pragma once
class A
{
public:
	A();
	~A();
	A(const A&) = delete;
	A& operator=(const A&) = delete;
	static A& Clone(A*);
private:
	int value;

};


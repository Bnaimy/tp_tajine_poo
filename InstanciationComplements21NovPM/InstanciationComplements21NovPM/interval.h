#pragma once
class interval
{
public:
	interval(float,float);
	~interval();
	interval operator+(const interval&) const; // I+J : I.operator+(j)
	interval operator-(const interval&) const;
	interval operator*(const interval&) const;
	static void main();
	void print() const;
	interval operator*(float ) const;
private:
	float binf;
	float bsup;

};


float min(float, float);
float max(float, float);